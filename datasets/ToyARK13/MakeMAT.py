'''
Goal
------
Make MAT file with fields
* nDoc
* doc_range
* X
* Xprev (if present)
'''

import bnpy
import ToyARK13

def Make_data_struct():
  Data = ToyARK13.get_data()
  Data.save_to_mat('HMMdataset.mat')
  

if __name__ == '__main__':
  Make_data_struct()
