function [stateSeq INDS stateCounts] = loadInitialStateSeq(data_struct, initpath, Kz)
Ks = 1;

if exist(initpath, 'dir')
   initpath = fullfile(initpath, 'Lap0000.000MAPStateSeqs.mat'); 
end
if ~exist(initpath, 'file')
    disp('Sampler code currently cannot be initialized on its own.');
    disp('Instead, we require memoized variational inference first,');
    disp('which will save a segmentation to disk that sampler code reads from.');
    error(['Required path for initialization not found.\n initpath=\n' initpath]); 
end
% Load initial z sequence from file
SVars = load(initpath);

if ~isfield(data_struct(1),'test_cases')
    data_struct(1).test_cases = [1:length(data_struct)];
end

% Initialize state count matrices:
N = zeros(Kz+1,Kz);
Ns = zeros(Kz,Ks);

% Preallocate INDS
for ii = 1:length(data_struct)
    T = length(data_struct(ii).blockSize);
    INDS(ii).obsIndzs(1:Kz,1:Ks) = struct('inds',sparse(1,T),'tot',0);
end

for ii=data_struct(1).test_cases
    T = length(data_struct(ii).blockSize);
    blockSize = data_struct(ii).blockSize;
    blockEnd = data_struct(ii).blockEnd;

    totSeq = zeros(Kz,Ks);
    indSeq = zeros(T,Kz,Ks);
    
    % Initialize state and sub-state sequences:
    z = 1+double(SVars.zHatBySeq{ii,1});
    s = ones(1,sum(blockSize));
    
    for t=1:T
        % Sample z(t):
        if (t == 1)
            obsInd = [1:blockEnd(1)];
        else
            obsInd = [blockEnd(t-1)+1:blockEnd(t)];
        end
        
        % Add state to counts matrix:
        if (t > 1)
            N(z(t-1),z(t)) = N(z(t-1),z(t)) + 1;
        else
            N(Kz+1,z(t)) = N(Kz+1,z(t)) + 1;  % Store initial point in "root" restaurant Kz+1
        end
        
        % Sample s(t,1)...s(t,Nt) and store sufficient stats:
        for k=1:blockSize(t)            
            % Add s(t,k) to count matrix and observation statistics:
            Ns(z(t),s(obsInd(k))) = Ns(z(t),s(obsInd(k))) + 1;
            totSeq(z(t),s(obsInd(k))) = totSeq(z(t),s(obsInd(k))) + 1;
            indSeq(totSeq(z(t),s(obsInd(k))),z(t),s(obsInd(k))) = obsInd(k);
        end
    end
    
    
    
    for jj = 1:Kz
        for kk = 1:Ks
            INDS(ii).obsIndzs(jj,kk).tot  = totSeq(jj,kk);
            INDS(ii).obsIndzs(jj,kk).inds = sparse(indSeq(:,jj,kk)');
        end
    end
    
    stateSeq(ii).z = z;
    stateSeq(ii).s = s;
end

binNs = zeros(size(Ns));
binNs(find(Ns)) = 1;
uniqueS = sum(binNs,2);

stateCounts.uniqueS = uniqueS;
stateCounts.N = N;
stateCounts.Ns = Ns;

return;
