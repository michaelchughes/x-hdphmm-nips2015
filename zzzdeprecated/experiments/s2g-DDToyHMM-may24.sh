for K in 1 4 16 64
do

for hmmKappa in 100
do

for creationProposalName in mixture randwindows
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --dataName DDToyHMM \
  --K $K \
  --initname randcontigblocks \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --creationProposalName $creationProposalName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may24 \
  --doFullPassBeforeMstep 0 \
  --queue short 
  
done
done
done
done
