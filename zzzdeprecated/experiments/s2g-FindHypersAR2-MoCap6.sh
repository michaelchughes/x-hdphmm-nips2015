dataName='MoCap6'
D=12
ECovMat='diagcovfirstdiff'
VMat='same'
MMat='eye'

nu=14

for hmmKappa in 0 100 300
do

for algName in bnpyHDPHMMdelmerge bnpyHDPHMMcreateanddestroy
do

for sF in 0.6 0.8 1.0
do
sV=$sF

for init in truelabels subdividetruelabels repeattruelabels 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'subdividetruelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --dataName $dataName \
  --K $K \
  --initname $initname \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --doFullPassBeforeMstep 1 \
  --nTask 5 \
  --nLap 50  \
  --minLaps 40 \
  --nu $nu \
  --ECovMat $ECovMat \
  --sF $sF \
  --VMat $VMat \
  --sV $sV \
  --MMat $MMat \
  --queue short \
  --jobname_final "findhyp3-alg=$algName-hmmKappa=$hmmKappa-nu=$nu-ECovMat=$ECovMat-sF=$sF-VMat=$VMat-MMat=$MMat-initname=$initname"

done
done
done
done
