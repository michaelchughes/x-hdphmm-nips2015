for K in 1 50
do

for hmmKappa in 100
do

for creationProposalName in bisectGrownBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --nickName june3smallerKmax \
  --dataName MoCap124 \
  --nBatch 20 \
  --K $K \
  --Kmax 100 \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --doFullPassBeforeMstep 1 \
  --nTask 2 \
  --nLap 1000 \
  --minLaps 400 \
  --minBlockSize 10 \
  --maxBlockSize 100 \
  --growthBlockSize 25 \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 2 \
  --creationLapDelim_late 200 \
  --creationNumProposal_early 0.2 \
  --creationNumProposal_late 0.1 \
  --creationKfresh_early 2 \
  --creationKfresh_late 2 \
  --nRefineIters 2 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 3 \
  $*

done
done
done
done
