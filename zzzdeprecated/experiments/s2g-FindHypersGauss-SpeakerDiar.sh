dataName='SpeakerDiar'
ECovMat='diagcovdata'
MMat='zero'

algName=bnpyHDPHMMcreateanddestroy
creationProposalName=bisectExistingBlocks

for meetingNum in 16 1 2 3 4 5
do

for sF in 0.1 0.5 1
do

for nu in 21 50 
do

for init in 'truelabels' '25' 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --dataName $dataName \
  --meetingNum $meetingNum \
  --K $K \
  --initname $initname \
  --hmmKappa 0 \
  --algName  $algName \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 10 \
  --creationLapDelim_late 20 \
  --creationKfresh_early 4 \
  --creationNumProposal_early 10 \
  --nTask 1 \
  --nLap 50 \
  --minLaps 50 \
  --nu $nu \
  --sF $sF \
  --ECovMat $ECovMat \
  --jobname_final "hypersearch0-ECovMat=$ECovMat-nu=$nu-sF=$sF-MMat=$MMat-algName=$algName-initname=$initname"

done
done
done
done
