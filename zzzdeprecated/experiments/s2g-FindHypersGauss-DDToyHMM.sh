dataName='DDToyHMM'
D=2
ECovMat='eye'
MMat='zero'

for sF in  0.01 0.1 1
do

for nu in 4 40 400
do

for init in 'truelabels' 'repeattruelabels' #'25' 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --dataName $dataName \
  --K $K \
  --initname $initname \
  --hmmKappa 100 \
  --algName  bnpyHDPHMMdelmerge \
  --nTask 2 \
  --nLap 25 \
  --minLaps 25 \
  --nu $nu \
  --sF $sF \
  --qmachine none \
  --queue short \
  --ECovMat $ECovMat \
  --jobname_final "findhypers-ECovMat=$ECovMat-nu=$nu-sF=$sF-MMat-$MMat-initname=$initname"

done
done
done
