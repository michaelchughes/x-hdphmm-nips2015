dataName='ToyARK13'
D=2
ECovMat='eye'
VMat='same'
MMat='zero'


for nu in 5 20 50
do

for sF in  0.05 0.1 0.2 0.4
do
sV=$sF

for init in repeattruelabels #25' 
do

  if [[ $init == 'truelabels' ]]; then
     K=-1
     initname=$init
  elif [[ $init == 'repeattruelabels' ]]; then
     K=-1
     initname=$init
  else
     K=$init
     initname='randcontigblocks'
  fi

python LaunchRun.py \
  --launchCode $1 \
  --dataName $dataName \
  --K $K \
  --initname $initname \
  --hmmKappa 100 \
  --algName  bnpyHDPHMMdelmerge \
  --nTask 1 \
  --nLap 10 \
  --minLaps 10 \
  --nu $nu \
  --ECovMat $ECovMat \
  --sF $sF \
  --VMat $VMat \
  --sV $sV \
  --MMat $MMat \
  --nSeq 26 \
  --T 500 \
  --jobname_final "findhypers-nu=$nu-ECovMat=$ECovMat-sF=$sF-VMat=$VMat-sV=$sV-MMat-$MMat-initname=$initname"

done
done
done
