for K in 1 4 16 64
do

for hmmKappa in 100
do

for creationProposalName in randBlocks #bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --doFullPassBeforeMstep 1 \
  --dataName DDToyHMM \
  --K $K \
  --initname randcontigblocks \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --creationProposalName $creationProposalName \
  --nTask 4 \
  --launchCode $1 \
  --nickName may27 \
  --queue short 
  
done
done
done
done
