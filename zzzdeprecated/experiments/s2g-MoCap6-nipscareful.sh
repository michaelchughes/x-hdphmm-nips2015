for K in 1
do

for hmmKappa in 100
do

for creationProposalName in randBlocks #bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --nickName nipsverycareful-tryagain \
  --doFullPassBeforeMstep 1 \
  --dataName MoCap6 \
  --nBatch 6 \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 5 \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 2 \
  --creationLapDelim_late 200 \
  --nLap 1000 \
  --minLaps 400 \
  --minBlockSize 10 \
  --maxBlockSize 100 \
  --creationKfresh_early 1 \
  --creationKfresh_late 1 \
  --creationNumProposal_early 1 \
  --creationNumProposal_late 1 \
  --nRefineIters 3 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 5 \
  --debug interactive \
  $*

done
done
done
done
