for K in 1 10 50
do

for hmmKappa in 0
do

for creationProposalName in bisectExistingBlocks
do

for algName in bnpyHDPHMMcreateanddestroy
do

python LaunchRun.py \
  --nickName nipsfinal \
  --dataName DDToyHMM \
  --K $K \
  --hmmKappa $hmmKappa \
  --algName $algName \
  --nTask 10 \
  --creationProposalName $creationProposalName \
  --creationLapDelim_early 2 \
  --creationLapDelim_late 20 \
  --creationNumProposal_early 5 \
  --creationNumProposal_late 1 \
  --nRefineIters 1 \
  --nGlobalIters 1 \
  --nGlobalItersBigChange 5 \
  $*

done
done
done
done
