N 6
Ktrue 12
maxT 380
BNPYDATADIR $XHMMROOT/datasets/$dataName/

nBatch 1
nLap 1000
nLap_test 5

saveEvery 10
saveEveryLogScaleFactor 2

obsModelName AutoRegGauss
ECovMat diagcovfirstdiff
sF 0.5
VMat same
sV 0.5
MMat eye
