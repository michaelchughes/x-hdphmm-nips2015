if [[ -z $deleteStartLap ]]; then
  deleteStartLap=1
fi

if [[ -z $dtargetMaxSize ]]; then
  dtargetMaxSize=10 # units here are number of sequences
fi

if [[ -z $deleteFailLimit ]]; then
  deleteFailLimit=3
fi

export BNPYDelPrefs="\
  --dtargetMinCount 0.01 \
  --dtargetMaxSize $dtargetMaxSize \
  --deleteNumStuckBeforeQuit 5 \
  --deleteFailLimit $deleteFailLimit \
  --deleteStartLap $deleteStartLap \
  "
