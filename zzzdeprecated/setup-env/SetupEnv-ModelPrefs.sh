export alpha=0.5
export gamma=10
if [[ -z $hmmKappa ]]; then
  export hmmKappa=0
fi

if [[ -z $ECovMat ]]; then
  export ECovMat='eye'
fi
if [[ -z $sF ]]; then
  export sF=1e-3
fi
if [[ -z $VMat ]]; then
  export VMat=eye;
fi
if [[ -z $MMat ]]; then
  export MMat=zero;
fi
if [[ -z $sM ]]; then
  export sM=1.0;
fi
if [[ -z $kappa ]]; then
  export kappa=1e-5
fi

export HDPPrefs=" \
 --startAlpha 5.0 \
 --alpha $alpha \
 --gamma $gamma \
 --hmmKappa $hmmKappa \
 "

export BNPYModelPrefs=" \
  $HDPPrefs \
  --ECovMat $ECovMat \
  --sF $sF \
  --kappa $kappa \
  --sV $sF \
  --VMat $VMat \
  --MMat $MMat \
  --sM $sM \
  "

