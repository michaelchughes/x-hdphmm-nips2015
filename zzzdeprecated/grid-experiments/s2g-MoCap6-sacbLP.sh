## Script for launching jobs on the grid.
# Usage
# --------------
# s2g____.sh <no args>
#    print out list of jobs that will be run
# s2g____.sh launch
#    send all jobs to the grid

dataName='MoCap6'
export jobName='Friday'
export runCode=$1
export OMP_NUM_THREADS=4

QTYPE=short
NTASK=1-11

export aModel=HDPHMM
export obsModel=AutoRegGauss
export ECovMat=diagcovfirstdiff
export sF=0.5
export VMat=same
export MMat=eye
export nBatch=1
export deleteStartLap=10
export mergeStartLap=5
export initname=sacbLP
export K=-1

for hmmKappa in 300 #0 100
do
  export hmmKappa=$hmmKappa

for initBlockLen in 10 20
do
  export initBlockLen=$initBlockLen
  echo "-------------------- hmmKappa $hmmKappa | initBlockLen $initBlockLen"

# Remember: Need to run with fixed before sampler
for alg in sampler #fixed delmerge #sampler #merge delmerge
do

  CMD="../Run_$aModel$alg.sh"
  CMD="$CMD $dataName $jobName 1 $initname $K $obsModel $ECovMat $sF $nBatch"

  if [[ -n $runCode ]] && [[ $runCode == 'test' ]]; then
    qsub -t 1 -l test $CMD
  elif [[ -n $runCode ]] && [[ $runCode == 'launch' ]]; then
    qsub -t $NTASK -l $QTYPE -q "*@@liv" $CMD
  else
    echo $CMD
  fi
  echo " "

done
done
done
exit
