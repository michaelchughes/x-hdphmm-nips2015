global dataName
dataName = 'MoCap124'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 205]
  Kticks = [0, 50, 100, 150, 200]

  Laplims = [0, 2100]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.8]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]

  ELBOlims = [-2.8, -2.38]
  ELBOticks = [-2.7, -2.6, -2.5, -2.4]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet + GreenSet[::-1] + OrangeSet + PurpleSet[::-1]
    C = np.asarray(Colors)/255.0;
    L = 10
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['delmerge'] = 'r'
    ColorMap['randBlocks'] = 'm'
    ColorMap['bisect'] = 'c'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=1'] = '-'
    StyleMap['K=50'] = '--'
    StyleMap['K=100'] = '--'
    StyleMap['K=200'] = '-'
    return StyleMap

kappaVals = ['100']
KVals = ['100', '200']

algNameVals = [
  #'foxHDPHMMsampler',
  'bnpyHDPHMMstoch',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'

def makeKeyPathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=%s %s Sticky=%s'
    path = "finalbaselines-alg=%s-lik=AutoRegGauss-hmmKappa=%s"
    path += "-ECovMat-diagcovfirstdiff-sF=0.5-VMat=same-MMat-eye"
    path += "-K=%s-initname=randcontigblocks-nBatch=20"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (K, algName, kappa)

                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    J[jobkey] = jobpath_tmp
                    #if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                    #    J[jobkey] = jobpath_tmp.replace('nBatch=6', 'nBatch=1')
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    


creationProposalNameVals = [
  'randBlocks',
  'bisectExistingBlocks',
  ]
def makeKeyPathMap_ForBirths():
    J = OrderedDict()
    key = 'K=1 %s'
    path = "nipsfinal-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=1-initname=randcontigblocks-nBatch=20"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for cpName in creationProposalNameVals:
                jobpath = path % (cpName)
                jobkey = key % (cpName.replace('Existing', ''))
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
cpNames = ['randBlocks', 'bisectGrownBlocks']
#june2numPropEarly10-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=1-initname=randcontigblocks-nBatch=20-creationProposalName=bisectGrownBlocks-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye
def makeKeyPathMap_ForBirths10():
    J = OrderedDict()
    key = 'K=1 %s 10'
    path = "june2numPropEarly10-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=1-initname=randcontigblocks-nBatch=20"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for cpName in cpNames:
                jobpath = path % (cpName)
                jobkey = key % (cpName.replace('Existing', ''))
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
cpNames = ['randBlocks', 'bisectGrownBlocks']
#june2numPropEarly10-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=1-initname=randcontigblocks-nBatch=20-creationProposalName=bisectGrownBlocks-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye
def makeKeyPathMap_ForBirths5():
    J = OrderedDict()
    key = 'K=1 %s 10'
    path = "june2numPropEarly5-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=1-initname=randcontigblocks-nBatch=20"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for cpName in cpNames:
                jobpath = path % (cpName)
                jobkey = key % (cpName.replace('Existing', ''))
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
# june3smallerKmax-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=50-initname=randcontigblocks-nBatch=20-creationProposalName=bisectGrownBlocks-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye
def makeKeyPathMap_ForBirthsKmax():
    J = OrderedDict()
    key = 'K=%s bisect Kmax=100'
    path = "june3smallerKmax-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=%s-initname=randcontigblocks-nBatch=20"
    path += "-creationProposalName=bisectGrownBlocks"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for K in ['1', '50']:
                jobpath = path % (K)
                jobkey = key % (K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J


# june4more-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-K=50-initname=randcontigblocks-nBatch=20-creationProposalName=bisectGrownBlocks-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye
def makeKeyPathMap_ForBirthsMore():
    J = OrderedDict()
    key = 'K=%s bisect Kmax=100'
    path = "june4more-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "K=%s-initname=randcontigblocks-nBatch=20"
    path += "-creationProposalName=bisectGrownBlocks"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for K in ['1', '50']:
                jobpath = path % (K)
                jobkey = key % (K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    

def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

