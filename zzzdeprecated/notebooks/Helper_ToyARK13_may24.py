global dataName
dataName = 'ToyARK13'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 100]
  Kticks = [13, 25, 50, 75, 100]
  Laplims = [-2, 200]
  Lapticks = [1, 10, 100]
  Hlims = [-.025, 0.8]
  Hticks = [0, 0.2, 0.4, 0.6]
  ELBOlims = [1.25, 1.45]
  ELBOticks = [1.3, 1.4]

  # Size of state seq plots
  ZW = 12 
  ZH = 3.5
  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks, 
                 ELBOlims, ELBOticks,
                 ZW, ZH)
BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet + GreenSet[::-1] + OrangeSet + PurpleSet[::-1]
    Colors.append([127,127,127])
    C = np.asarray(Colors)/255.0;
    L = 100
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getColorMap():
  ShadesOfRed = [
    '#fee5d9',
    '#fcae91',
    '#fb6a4a',
    '#fb6a4a',
    '#cb181d',
    ]
  ColorKeys = ['K=1', 'K=4', 'K=16', 'K=64']
  ColorMap = OrderedDict()
  for keyPos, key in enumerate(ColorKeys):
      ColorMap[key] = ShadesOfRed[keyPos]
  return ColorMap

def getStyleMap():
  StyleMap = dict([('K', '-')])
  return StyleMap

def makeJobKeyPathMap(
        KVals='1,4,16,64',
        kappaVals='100',
        creationProposalNameVals='mixture'):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'create/delete/merge-K=%s'
    path = "may24-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-"
    path += "K=%s-initname=randcontigblocks-nBatch=13"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=eye-sF=0.1-VMat=eye-MMat=zero"
    for cpName in creationProposalNameVals.split(','):
        for kappa in kappaVals.split(','):
            for K in KVals.split(','):
                jobpath = path % (kappa, K, cpName)
                jobkey = key % (K)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    
def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

def getSubsetByName(JJ, name):
    JJ2 = OrderedDict()
    for key, path in JJ.items():
        if key.count(name):
            JJ2[key] = path
    return JJ2
