global dataName
dataName = 'MoCap6'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 65]
  Kticks = [0, 20, 40, 60]

  Laplims = [0, 2100]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.8]
  Hticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7]

  ELBOlims = None #[-2.2, -2.07]
  ELBOticks = None # [-2.2, -2.15, -2.1]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = [
        [166,206,227],
        [31,120,180],
        [178,223,138],
        [51,160,44],
        [251,154,153],
        [255,127,0],
        [253,191,111],
        [227,26,28],
        [202,178,214],
        [106,61,154],
        [255,255,153],
        [177,89,40],
        ]
    C = np.asarray(Colors)/255.0;
    L = 10
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        g = grayVals[L-ell-1]
        shadeOfGray = np.asarray([g, g, g])
        C = np.vstack([C, shadeOfGray[np.newaxis,:]] )
    L = 10
    grayVals = np.linspace(0.2, 0.9, L)
    for ell in xrange(L):
        g = grayVals[L-ell-1]
        shadeOfGray = np.asarray([g, g, g])
        C = np.vstack([C, shadeOfGray[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['delmerge'] = 'r'
    ColorMap['birth'] = 'm'
    ColorMap['randBlocks'] = 'm'
    ColorMap['bisect'] = 'c'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=1'] = '-'
    StyleMap['K=30'] = '--'
    StyleMap['K=60'] = '-'
    return StyleMap

kappaVals = ['0', '20', '100']
KVals = ['30', '60']

algNameVals = [
  'foxHDPHMMsampler',
  'bnpyHDPHMMstoch',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'

def makeKeyPathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=%s %s Sticky=%s'
    path = "finalbaselines-alg=%s-lik=AutoRegGauss-hmmKappa=%s"
    path += "-ECovMat-diagcovfirstdiff-sF=0.5-VMat=same-MMat-eye"
    path += "-K=%s-initname=randcontigblocks-nBatch=6"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (K, algName, kappa)

                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    J[jobkey] = jobpath_tmp
                    #if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                    #    J[jobkey] = jobpath_tmp.replace('nBatch=6', 'nBatch=1')
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J
    


creationProposalNameVals = [
  'bisectGrownBlocks',
  ]

initnameVals = [
  'K=1-initname=randcontigblocks',
  ]

nameVals = [
  'mergefirst',
  ]

def makeKeyPathMap_ForBirths(
        nameVals=nameVals,
        initnameVals=initnameVals,
        creationProposalNameVals=creationProposalNameVals):
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=1 birth'
    path = "june2%s-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100-"
    path += "%s-nBatch=6"
    path += "-creationProposalName=%s"
    path += "-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye"
    for cpName in creationProposalNameVals:
      for nameVal in nameVals:
        for iName in initnameVals:
                jobpath = path % (nameVal, iName, cpName)
                jobkey = key # % (nameVal, cpName)

                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J


def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

