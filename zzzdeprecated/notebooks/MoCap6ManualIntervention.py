import bnpy
import MoCap6
Data = MoCap6.get_data()

import os
import numpy as np
import glob
import PlotUtil as PU
import scipy.io
StateSeqUtil = bnpy.util.StateSeqUtil

def PrintContigBlocks(jobpath='', taskid='', 
                      zBySeq=None, seqIDs=[1,2,3,4,5,6],
                      zSuffix='Aligned', minBlockLen=5):
  if zBySeq is None:
    taskpath = PU.MakePath(os.path.join(jobpath, str(taskid)))
    print taskpath

    zpaths = glob.glob(os.path.join(taskpath, '*MAPStateSeqs' + zSuffix + '.mat'))
    zpaths.sort()
    zpath = zpaths[-1]
    zBySeq = scipy.io.loadmat(zpath)

    zBySeq = zBySeq['zHatBySeq' + zSuffix]
    zBySeq = bnpy.util.StateSeqUtil.convertStateSeq_MAT2list(zBySeq)


  seqIDs = np.asarray(seqIDs) -1 
  for seqID in seqIDs:
    print 'Seq #%d' % (seqID + 1)
    z_n = zBySeq[seqID]
    diffs = np.hstack([0, np.diff(z_n)])
    breakLocs = np.flatnonzero(diffs != 0)
    chunks = np.split(z_n, breakLocs)
    breakLocs = np.hstack([0, breakLocs])
    for cc, chunk in enumerate(chunks):
      startFrac = breakLocs[cc]/float(z_n.size)
      stopFrac = (breakLocs[cc] + chunk.size)/float(z_n.size)
      if chunk.size > minBlockLen:        
        print '  state=%d L=%d   startFrac %.2f stopFrac %.2f' \
                 % (chunk[0], chunk.size, startFrac, stopFrac)
  return zBySeq

  

def RunManualIntervention(jobpath, taskid, 
                          zBySeq=None, Kmax=None):
  taskpath = PU.MakePath(os.path.join(jobpath, str(taskid)))
  hmodel = bnpy.load_model(taskpath)
  print '------------ FROM CURRENT'
  RunForwardAndPrint(Data, hmodel.copy(), LP=None)
 
  Zflat = bnpy.util.StateSeqUtil.convertStateSeq_list2flat(zBySeq, Data)
  LP = dict(Z=Zflat)
  LP = bnpy.init.FromTruth.convertLPFromHardToSoft(LP, Data, 
                                                   startIDsAt0=True,
                                                   Kmax=Kmax)  
  LP = hmodel.allocModel.initLPFromResp(Data, LP)
  print '------------ FROM NEW'
  bestModel = RunForwardAndPrint(Data, hmodel.copy(), LP)

  return AlignModelAndCalcDistance(Data, bestModel)

def CheckViterbiVsSoft(jobpath, taskid):
  taskpath = PU.MakePath(os.path.join(jobpath, str(taskid)))
  hmodel = bnpy.load_model(taskpath)

  LPsoft = hmodel.calc_local_params(Data, limitMemoryLP=1)
  SSsoft = hmodel.get_global_suff_stats(Data, LPsoft)

  #zHatBySeq = AlignModelAndCalcDistance(Data, hmodel)
  from bnpy.allocmodel.hmm.HMMUtil import runViterbiAlg
  initPi = hmodel.allocModel.get_init_prob_vector()
  transPi = hmodel.allocModel.get_trans_prob_matrix()
  LP = hmodel.obsModel.calc_local_params(Data)
  Lik = LP['E_log_soft_ev']
  # Loop over each sequence in the collection
  zHatBySeq = list()
  for n in range(Data.nDoc):
    start = Data.doc_range[n]
    stop = Data.doc_range[n+1]
    zHat = runViterbiAlg(Lik[start:stop], initPi, transPi)
    zHatBySeq.append(zHat)
  zHatFlat = StateSeqUtil.convertStateSeq_list2flat(zHatBySeq, Data)
  LP = dict(Z=zHatFlat)
  LP = bnpy.init.FromTruth.convertLPFromHardToSoft(LP, Data, 
                                                   startIDsAt0=True,
                                                   Kmax=SSsoft.K)
  LP = hmodel.allocModel.initLPFromResp(Data, LP)
  SShard = hmodel.get_global_suff_stats(Data, LP)
  return SShard, SSsoft, LPsoft


def RunForwardAndPrint(Data, hmodel, LP=None, nSteps=50):
  for iterid in xrange(nSteps):
    if iterid > 0 or LP is None:
      LP = hmodel.calc_local_params(Data, limitMemoryLP=1)
    if 'Htable' not in LP:
      SS = hmodel.get_global_suff_stats(Data, LP)
    else:
      SS = hmodel.get_global_suff_stats(Data, LP, doPrecompEntropy=1)
    if iterid == 0:
      print ' '.join(['%.2f' % x for x in SS.N])

    emptyIDs = np.flatnonzero(SS.N < 0.0001)
    if np.any(emptyIDs):
      print 'Delete!', emptyIDs[0]
      SS.removeComp(emptyIDs[0])
    hmodel.update_global_params(SS)
    if (iterid < 3 or iterid % 10 == 0) and SS.hasELBOTerms():
      ELBO = hmodel.calc_evidence(SS=SS)
      print '%.3f' % (ELBO)
  print ' '.join(['%.2f' % x for x in SS.N])
  return hmodel



def AlignModelAndCalcDistance(Data, hmodel):
  from bnpy.allocmodel.hmm.HMMUtil import runViterbiAlg
  initPi = hmodel.allocModel.get_init_prob_vector()
  transPi = hmodel.allocModel.get_trans_prob_matrix()

  LP = hmodel.obsModel.calc_local_params(Data)
  Lik = LP['E_log_soft_ev']

  # Loop over each sequence in the collection
  zHatBySeq = list()
  for n in range(Data.nDoc):
    start = Data.doc_range[n]
    stop = Data.doc_range[n+1]
    zHat = runViterbiAlg(Lik[start:stop], initPi, transPi)
    zHatBySeq.append(zHat)
  zHatFlat = StateSeqUtil.convertStateSeq_list2flat(zHatBySeq, Data)
  zHatFlatAligned = \
      StateSeqUtil.alignEstimatedStateSeqToTruth(zHatFlat, Data.TrueParams['Z'])
  zHatBySeqAligned = StateSeqUtil.convertStateSeq_flat2list(
                                    zHatFlatAligned, Data)

  zTrue = Data.TrueParams['Z']

  hdistance = StateSeqUtil.calcHammingDistance(zHatFlatAligned, zTrue)
  normhdist = float(hdistance) / float(zHatFlatAligned.size)
  print 'distance=', normhdist
  return zHatBySeqAligned
