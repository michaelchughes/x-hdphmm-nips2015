global dataName
dataName = 'BigChromatinCD4T'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

def setUp():
  Klims = [-0.5, 120]
  Kticks = [0, 50, 100]

  Laplims = [-1, 60]
  Lapticks = [0, 10, 20, 30, 40, 50]

  Hlims = None
  Hticks = None
  ELBOlims = None
  ELBOticks = None

  # Size of state seq plots
  ZW = 12
  ZH = 4.5 # Dont shrink this. Text gets cut off!
  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  return makeKeyPathMap()

BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet[1:3] + GreenSet[1:3] + OrangeSet[1:3] + PurpleSet[1:3]
    C = np.asarray(Colors)/255.0;
    L = 4
    grayVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([grayVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['randBlocks'] = 'c'
    ColorMap['bisect'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=50'] = '--'
    StyleMap['K=100'] = '-'
    StyleMap['K=1'] = '-'
    return StyleMap


KVals = ['50', '100']

algNameVals = [
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
# may31-alg\=bnpyHDPHMMmemo-lik\=Bern-lam1\=0.1-lam0\=0.3-hmmKappa\=100-K\=
def makeKeyPathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "alg1-Stick=0"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = 'K=%s alg=%s'
    path = "may31-alg=%s-lik=Bern-lam1=0.1-lam0=0.3-hmmKappa=100"
    path += "-K=%s-initname=randcontigblocks-nBatch=173"
    for algName in algNameVals:
            for K in KVals:
                jobpath = path % (algName, K)
                jobkey = key % (K, algName)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makeKeyPathMap_stoch():
    J = OrderedDict()
    key = 'K=%s alg=stoch'
    path = "may31-alg=bnpyHDPHMMstoch-hmmKappa=100-lik=Bern-lam1=0.1-lam0=0.3"
    path += "-K=%s-initname=randcontigblocks-nBatch=173"
    path += "-lRateDelay=1-lRatePower=0.51"
    for K in KVals:
        jobkey = key %(K)
        jobpath = path % (K)
        J[jobkey] = jobpath
    return J

creationProposalNames = [
  'randBlocks', 'bisectExistingBlocks']
def makeKeyPathMap_birth():
    J = OrderedDict()
    key = 'K=1 proposal=%s'
    path = "may31-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=100"
    path += "-K=1-initname=randcontigblocks-nBatch=173"
    path += "-creationProposalName=%s"
    path += "-lik=Bern-lam1=0.1-lam0=0.3"
    for cpName in creationProposalNames:
        jobkey = key %(cpName)
        jobpath = path % (cpName)
        J[jobkey] = jobpath
    return J
























def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

