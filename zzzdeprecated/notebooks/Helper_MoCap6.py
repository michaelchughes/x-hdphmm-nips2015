global dataName
dataName = 'MoCap6'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 68]
  Kticks = [20, 40, 60]

  Laplims = [0, 1400]
  Lapticks = [2, 10, 100, 1000]

  Hlims = [0.1, 0.8]
  Hticks = [0.2, 0.4, 0.6]
  # Size of state seq plots
  ZW = 12
  ZH = 4.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks,
                 ZW, ZH)

## initname=truelabels
J = dict()
prefix = 'Wed-alg=HDPHMMfixed-hmmKappa=300-lik=AutoRegGauss-ECovMat=diagcovfirstdiff-sF=0.5-VMat=same-MMat=eye-'
J['fixed-rcontigblocks-Kinit=32'] = prefix + 'randcontigblocks-initBlockLen=20-Kinit=32-nBatch=1'
J['fixed-rcontigblocks-Kinit=64'] = prefix + 'randcontigblocks-initBlockLen=20-Kinit=64-nBatch=1'

def getSubsetByName(JJ, name):
  JJ2 = OrderedDict()
  for key, path in JJ.items():
    if key.count(name):
      JJ2[key] = path
  return JJ2

extraAlgNames = ['delmerge', 'sampler', 'stoch']
extraAlgSuffix = ['-mergeStartLap=5','', '']

def getJobPathsByKeys():
  global J
  JJ = copy.deepcopy(J)
  for key, path in JJ.items():  
    if not os.path.exists(PlotUtil.MakePath(path)):
      print 'CANNOT FIND USER-SPECIFIED PATH: ', path
      continue

    for ii, algName in enumerate(extraAlgNames):
      rkey = key.replace('fixed', algName)
      rpath = path.replace('fixed', algName)
      if not os.path.exists(PlotUtil.MakePath(rpath)):
        rpath_tmp = rpath + extraAlgSuffix[ii]
        rpath_tmp2 = rpath.replace('stoch', 'stoch-learnRate=aa')
        rpath_tmp3 = rpath.replace('Wed-alg=HDPHMMsampler',
                                   'Wed-draft-alg=HDPHMMsampler')
        if os.path.exists(PlotUtil.MakePath(rpath_tmp)):
          rpath = rpath_tmp
        elif os.path.exists(PlotUtil.MakePath(rpath_tmp2)):
          rpath = rpath_tmp2    
        elif os.path.exists(PlotUtil.MakePath(rpath_tmp3)):
          rpath = rpath_tmp3
        else:
          print 'CANNOT FIND AUTO-GENERATED PATH: ', rpath_tmp
          continue
      JJ[rkey] = rpath
  # Sort by keys
  return OrderedDict(sorted(JJ.items()))

def getColorMap():
  ColorMap = OrderedDict()
  ColorMap['fixed'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  ColorMap['truelabels'] = 'y'
  ColorMap['repeattrue'] = 'm'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('8', '-'), ('16', ':'), ('32','-'), ('64', '--'),
                   ('truelabels', '-'), ('repeat', ':'),
                   ('B=10', '-'), ('B=15', ':')])
  return StyleMap
