global dataName
dataName = 'DDToyHMM'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil

def setUp():
  Klims = [0, 67]
  Kticks = [8, 16, 32, 64]

  Laplims = [0, 2100]
  Lapticks = [2, 10, 100, 1000]
  #Laplims = [0, 1000]
  #Lapticks = [2, 10, 100, 400]

  Hlims = [-.025, 0.8]
  Hticks = [0, 0.2, 0.4, 0.6]

  # Size of state seq plots
  ZW = 12
  ZH = 3.5

  PlotUtil.setUp(dataName, getColorMap(), getStyleMap(), 
                 Klims, Kticks, Laplims, Lapticks, Hlims, Hticks,
                 ZW, ZH)

## initname=truelabels
J = dict()
key = 'fixed-randbydist-Kinit=%d'
path = "ThursLong-alg=HDPHMMfixed-hmmKappa=0-lik=Gauss-ECovMat=eye-sF=1-randexamplesbydist-Kinit=%d-nBatch=10"
for K in [16, 32, 64]:
  J[key % K] = path % (K)


def getSubsetByName(JJ, name):
  JJ2 = OrderedDict()
  for key, path in JJ.items():
    if key.count(name):
      JJ2[key] = path
  return JJ2

def getJobPathsByKeys():
  global J
  JJ = copy.deepcopy(J)
  for key, path in JJ.items():  
    if not os.path.exists(PlotUtil.MakePath(path)):
      print 'CANNOT FIND USER-DEF PATH: ', path
      continue
    for newname in ['sampler', 'stoch', 'delmerge']:
      rkey = key.replace('fixed', newname)
      rpath = path.replace('fixed', newname)
      if newname == 'stoch':
        rpath = path.replace('fixed', 'stoch-learnRate=aa')

      if not os.path.exists(PlotUtil.MakePath(rpath)):
        print 'CANNOT FIND AUTO-GEN PATH: ', rpath
        continue
      JJ[rkey] = rpath
  # Sort by keys
  return OrderedDict(sorted(JJ.items()))

def getColorMap():
  ColorMap = dict()
  ColorMap['fixed'] = '#0868ac' # deep blue
  ColorMap['sampler'] = '#1b9e77' # teal
  ColorMap['stoch'] = '#f6c141' # sea green
  ColorMap['delmerge'] = 'r'
  return ColorMap

def getStyleMap():
  StyleMap = dict([('8', '-'), ('16', ':'), ('32','-'), ('64', '--')])
  return StyleMap
