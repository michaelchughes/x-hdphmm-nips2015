global dataName
dataName = 'BigChromatinCD4T'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

# Define value ranges used for experiments
kappaVals = ['100']
KVals = ['50', '100']
KVals_Birth = ['1']
suffix = '-lRateDelay=1-lRatePower=0.51'


def setUp():
  Klims = [-0.5, 120]
  Kticks = [0, 25, 50, 75, 100]

  Laplims = [0, 105]
  Lapticks = [1, 10, 100]

  Hlims = None
  Hticks = None

  ELBOlims = [-0.0365, -0.033]
  ELBOticks = [-0.036, -0.035, -0.034]

  # Size of state seq plots
  ZW = 8
  ZH = 4.5 # Dont shrink this. Text gets cut off!

  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  J = OrderedDict()
  J.update(makePathMap_ForStoch())
  J.update(makePathMap())
  J.update(makePathMap_ForBirths())
  return J


def getStateSeqColorMap(nErrorStates=20):
    ''' Create colormap with entry for each of the 8 true states.

    Extraneous state indices are assigned to different shades of red.

    Returns
    -------
    Cmap : ListedColormap
    '''
    from matplotlib.colors import ListedColormap
    C = np.asarray([[0,0,0]])/255.0;
    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['birth'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=1'] = '-'
    StyleMap['K=50'] = '--'
    StyleMap['K=100'] = '-'
    return StyleMap

def makePathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "memo Sticky=50 K=50"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s Sticky=%s K=%s'
    path = "nipsexperimentlonger-alg=%s-lik=Bern-lam1=0.1-lam0=0.3"
    path += "-hmmKappa=%s-K=%s"
    path += "-initname=randcontigblocks-nBatch=173"
    for algName in ['bnpyHDPHMMmemo']:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (algName, kappa, K)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makePathMap_ForStoch():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "memo Sticky=50 K=50"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s Sticky=%s K=%s'
    path = "nipsexperimentlonger-alg=%s-hmmKappa=%s"
    path += "-lik=Bern-lam1=0.1-lam0=0.3"
    path += "-K=%s"
    path += "-initname=randcontigblocks-nBatch=173"
    path += suffix
    for algName in ['bnpyHDPHMMstoch']:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (algName, kappa, K)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makePathMap_ForBirths():
    J = OrderedDict()
    key = 'birth Sticky=%s K=%s'
    path = "nipsexperiment-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s"
    path += "-K=%s"
    path += "-initname=randcontigblocks-nBatch=173"
    path += "-creationProposalName=bisectGrownBlocks"
    path += "-lik=Bern-lam1=0.1-lam0=0.3"
    for kappa in kappaVals:
        for K in KVals_Birth:
                jobpath = path % (kappa, K)
                jobkey = key % (kappa, K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key %s does not have valid path' % (jobkey)
                    print jobpath
    return J

def makeLegendInOwnFigure(
        Jdict,
        names = ['stoch', 'sampler', 'memo', 
                 'delete,merge', 'birth,delete,merge'],
    ):
    ''' Create legend for trace plots, in separate matplotlib figure.
    '''
    # Split up each name into original name (key in Jdict) and legend name
    origNames = list()
    legendNames = list()
    for name in names:
        if name.count(":") > 0:
            fields = name.split(":")
            origNames.append(fields[0])
            legendNames.append(fields[1])
        else:
            origNames.append(name)
            legendNames.append(name)            
    # Build job dict with one entry for each of the specified names
    J2 = OrderedDict()
    for name in origNames:
        Jmatch = getSubsetByName(Jdict, name, keepName=1)
        if len(Jmatch.keys()) == 0:
            raise ValueError("Cannot find key %s in provided job path dict" % (
                name))
        firstkey = Jmatch.keys()[0]
        Jsingle = dict()
        Jsingle[name + firstkey] = Jmatch[firstkey]
        J2.update(Jsingle)
    # Make trace plot with these jobs only
    PlotUtil.plotK(J2, loc=None, xscale='log')
    # Grab current axes of this trace plot
    axH = PlotUtil.pylab.gca();
    # Set all line handles to wide, solid lines
    lineHandles, lineLabels = axH.get_legend_handles_labels()
    for ii in range(len(lineHandles)):
        lineHandles[ii].set_linewidth(6)
        if legendNames[ii].count('K='):
            lineHandles[ii].set_linewidth(3)
            lineHandles[ii].set_color('black')
        else:
            lineHandles[ii].set_linestyle('-')
    # Remove lines from the plot entirely, so its just the legend
    axH.axis('off')
    for ii in range(len(axH.lines)):
        axH.lines.pop()
    # Show the legend
    axH.legend(lineHandles, legendNames)

def isValidJobKeyAndPath(jobkey, jobpath):
    if not os.path.exists(PlotUtil.MakePath(jobpath)):
        return False
    return True

