global dataName
dataName = 'SpeakerDiar'

from collections import OrderedDict
import numpy as np
import copy
import os
import PlotUtil
from PlotUtil import getSubsetByName

import bnpy
from bnpy.viz.JobFilter import makePPListMapFromJPattern

# Define value ranges used for experiments
kappaVals = ['100']
KVals = ['25']
KVals_Birth = ['25']
algNameVals = [
  'foxHDPHMMsampler',
  'bnpyHDPHMMmemo',
  'bnpyHDPHMMdelmerge',
  ]
suffix = '-lRateDelay=1-lRatePower=0.51'


BlueSet = [
    [222,235,247],
    [158,202,225],
    [ 49,130,189],
    ]
GreenSet = [
    [229,245,224],
    [161,217,155],
    [ 49,163, 84],
    ]
OrangeSet = [
    [255,247,188],
    [254,196, 79],
    [217, 95, 14],
    ]
PurpleSet = [
    [253,224,221],
    [250,159,181],
    [197, 27,138],
    ]

def setUp():
  Klims = [-0.5, 27]
  Kticks = [0, 5, 10, 15, 20, 25]

  Laplims = [0, 2100]
  Lapticks = [1, 10, 100, 1000]

  Hlims = [-0.03, 0.85]
  Hticks = [0, 0.2, 0.4, 0.6, 0.8]

  ELBOlims = None #[-1.65, -1.4]
  ELBOticks = None #[-1.6, -1.55, -1.5, -1.45]

  # Size of state seq plots
  ZW = 8
  ZH = 4.5 # Dont shrink this. Text gets cut off!

  PlotUtil.setUp(dataName, 
      getStateSeqColorMap(), getLineColorMap(), getLineStyleMap(), 
      Klims, Kticks, Laplims, Lapticks, 
      Hlims, Hticks, ELBOlims, ELBOticks,
      ZW, ZH)
  J = makePathMap()
  J.update(makePathMap_ForBirths())
  return J


def getStateSeqColorMap():
    from matplotlib.colors import ListedColormap
    Colors = BlueSet[2:3] + GreenSet[2:3] + OrangeSet[2:3] + PurpleSet[2:3] \
        + BlueSet[1:2] + GreenSet[1:2] + OrangeSet[1:2] + PurpleSet[1:2]

    C = np.asarray(Colors)/255.0;
    L = 10
    shadeVals = np.linspace(0.15, 0.85, L)
    for ell in xrange(L):
        shadeOfRed = np.asarray([shadeVals[L-ell-1], 0, 0])
        C = np.vstack([C, shadeOfRed[np.newaxis,:]] )

    # Add two gray states for bg state values -2 and -1
    C = np.vstack([
        0.3 * np.ones((1,3)), 
        0.6 * np.ones((1,3)), 
        C])

    Cmap = ListedColormap(C)
    return Cmap

def getLineColorMap():
    ColorMap = OrderedDict()
    ColorMap['memo'] = '#0868ac' # deep blue
    ColorMap['stoch'] = '#f6c141' # sea green
    ColorMap['sampler'] = '#1b9e77' # teal
    ColorMap['delmerge'] = 'r'
    ColorMap['birth'] = 'm'
    ColorMap['truelabels'] = '#ccac00' # metallic gold
    return ColorMap

def getLineStyleMap():
    StyleMap = OrderedDict()
    StyleMap['K=1'] = '-'
    StyleMap['K=25'] = '-'
    return StyleMap

def makePathMap():
    ''' Make dict mapping legend names to system paths where results stored.

    Returns
    -------
    J : Ordered Dict
        keys are legend-ready phrases like "memo Sticky=50 K=50"
        values are full paths to output directory where job dumped its info
    '''
    J = OrderedDict()
    key = '%s Sticky=%s K=%s'
    path = "nips2015-alg=%s-lik=Gauss-hmmKappa=%s"
    path += "-ECovMat-diagcovdata-sF=0.5"
    path += "-K=%s-initname=randcontigblocks-nBatch=1"
    for algName in algNameVals:
        for kappa in kappaVals:
            for K in KVals:
                jobpath = path % (algName, kappa, K)
                jobkey = key % (algName, kappa, K)
                jobkey = jobkey.replace('HDPHMM', '')
                jobkey = jobkey.replace('bnpy', '')
                jobkey = jobkey.replace('fox', '')
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                else:
                    jobpath_tmp = jobpath + suffix
                    if isValidJobKeyAndPath(jobkey, jobpath_tmp):
                        J[jobkey] = jobpath_tmp
                if jobkey not in J:
                    print 'Key without valid path: %s' % (jobkey)
                    print jobpath
    return J

def makePathMap_ForBirths():
    J = OrderedDict()
    key = 'birth Sticky=%s K=%s'
    bpath = "nips2015-alg=bnpyHDPHMMcreateanddestroy-hmmKappa=%s-K=%s"
    bpath += "-initname=randcontigblocks-nBatch=1"
    bpath += "-creationProposalName=bisectGrownBlocks"
    bpath += "-lik=Gauss-ECovMat=diagcovdata-sF=0.5"
    for kappa in kappaVals:
        for K in KVals_Birth:
                jobpath = bpath % (kappa, K)
                jobkey = key % (kappa, K)
                if isValidJobKeyAndPath(jobkey, jobpath):
                    J[jobkey] = jobpath 
                if jobkey not in J:
                    print 'Key without valid path: %s' % (jobkey)
                    print jobpath
    return J

def makeLegendInOwnFigure(
        Jdict,
        names = ['sampler', 'memo', 'delete,merge', 'birth,delete,merge'],
    ):
    ''' Create legend for trace plots, in separate matplotlib figure.
    '''
    # Split up each name into original name (key in Jdict) and legend name
    origNames = list()
    legendNames = list()
    for name in names:
        if name.count(":") > 0:
            fields = name.split(":")
            origNames.append(fields[0])
            legendNames.append(fields[1])
        else:
            origNames.append(name)
            legendNames.append(name)
    # Build job dict with one entry for each of the specified names
    J2 = OrderedDict()
    for name in origNames:
        Jmatch = getSubsetByName(Jdict, name, keepName=1)
        if len(Jmatch.keys()) == 0:
            raise ValueError("Cannot find key %s in provided job path dict" % (
                name))
        firstkey = Jmatch.keys()[0]
        Jsingle = dict()
        Jsingle[firstkey] = Jmatch[firstkey]
        J2.update(Jsingle)
    # Make trace plot with these jobs only
    PlotUtil.plotK(J2, loc=None, xscale='log', n='1')
    # Grab current axes of this trace plot
    axH = PlotUtil.pylab.gca();
    # Set all line handles to wide, solid lines
    lineHandles, lineLabels = axH.get_legend_handles_labels()
    for ii in range(len(lineHandles)):
        lineHandles[ii].set_linewidth(6)
        lineHandles[ii].set_linestyle('-')
    # Remove lines from the plot entirely, so its just the legend
    axH.axis('off')
    for ii in range(len(axH.lines)):
        axH.lines.pop()
    # Show the legend
    axH.legend(lineHandles, legendNames)

def isValidJobKeyAndPath(jobkey, jobpath, n='1'):
    if not os.path.exists(PlotUtil.MakePath(jobpath, n=n)):
        return False
    return True


def plotScatterComparison(jpath1, jpath2, nTask=10, pylab=PlotUtil.pylab):
    pylab.rcParams['pdf.fonttype'] = 42
    pylab.rcParams['ps.fonttype'] = 42
    pylab.rcParams['text.usetex'] = False

    pylab.hold('on');
    xs = np.linspace(-1, 1, 10);
    pylab.plot(xs, xs, 'k--');

    scores = dict()
    for n in range(21):
        path1 = PlotUtil.MakePath(jpath1, n=str(n+1))
        path2 = PlotUtil.MakePath(jpath2, n=str(n+1))
        scores[n] = np.zeros(nTask)
        for task in range(nTask):
            hpath1 = os.path.join(path1, str(task+1), 'hamming-distance.txt')
            dist1 = np.loadtxt(hpath1)[-1]
            hpath2 = os.path.join(path2, str(task+1), 'hamming-distance.txt')
            dist2 = np.loadtxt(hpath2)[-1]
            pylab.plot(dist1, dist2, 'k.', markersize=12)
            scores[n][task] = dist1 - dist2
    pylab.axis('image');
    pylab.xlim([-0.001, 0.61]);
    pylab.ylim([-0.001, 0.61]);
    pylab.xlabel('delete-merge Hamming');
    pylab.ylabel('sampler Hamming');
    print pylab.rcParams['ps.fonttype'], '<<'
    return scores

