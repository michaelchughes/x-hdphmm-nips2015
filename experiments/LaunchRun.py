#!/usr/bin/python
# Launch inference job applying specific alg to specific dataset.
# 
# Usage
# -----
# python LaunchRun.py --dataset ___ --algName ____ ...
# where many optional (--name value) args can be specified.
# 
# Required Env Vars
# -----------------
# XHOST : {'local','grid','testgrid'}
# * 'local' : runs the job on the local machine
# * 'grid' : runs job on the SUN Grid Engine, unlimited queue
# * 'testgrid' : runs job on the SUN Grid Engine, test queue

import argparse
import os
import sys
import commands
import subprocess
import tempfile
from collections import OrderedDict

ENVVARS = [
    'BNPYOUTDIR', 'XHMMROOT', 'BNPYDATADIR', 'BNPYLOGDIR',
    'PATH', 'PYTHONPATH', 'HOME', 'OMP_NUM_THREADS',
    'LIGHTSPEEDROOT', 'FOXMCMCROOT',
    ]

# Set default env variables
if 'BNPYOUTDIR' not in os.environ:
    raise ValueError('Expected environment variable BNPYOUTDIR not found.\n' + \
        'Follow instructions online:\n' + \
        'https://bitbucket.org/michaelchughes/bnpy-dev/wiki/Configuration.md'
        )

curpathparts = os.path.abspath(__file__).split(os.path.sep)
reporoot = os.path.sep.join(curpathparts[:-2])
if 'XHMMROOT' not in os.environ:
    os.environ['XHMMROOT'] = reporoot
if 'FOXMCMCROOT' not in os.environ:
    os.environ['FOXMCMCROOT'] = os.path.join(reporoot, 'code/fox-mcmc-hdphmm/')
if 'LIGHTSPEEDROOT' not in os.environ:
    os.environ['LIGHTSPEEDROOT'] = os.path.join(reporoot, 'code/lightspeed/')
if 'XHOST' not in os.environ:
    os.environ['XHOST'] = 'local'
if 'BNPYLOGDIR' not in os.environ:
    os.environ['BNPYLOGDIR'] = os.environ['BNPYOUTDIR']

def main(**kwargsIN):
    ''' Read job args from stdin and launch job.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('--printCMD', default=1, type=int)
    parser.add_argument('--algName', default='bnpyHDPHMMmemo')
    parser.add_argument('--dataName', default='DDToyHMM')
    parser.add_argument('--nickName', default='test')
    parser.add_argument('--nTask', default='1')
    parser.add_argument('--nWorkers', default='1')
    parser.add_argument('--verbose', default=False)
    parser.add_argument('--queue', default='liv')
    parser.add_argument('--qmachine', default=None)
    args, UNKlist = parser.parse_known_args()
    kwargs = dict(**args.__dict__)
    kwargs.update(arglist_to_dict(UNKlist))
    runJob(**kwargs)

def runJob(**kwargs):
    XHOST = os.environ['XHOST']
    kwargs['XHOST'] = XHOST
    CMD, CMDkwargs = makeJobCMD(**kwargs)
    os.environ['OMP_NUM_THREADS'] = '1'
    
    if CMDkwargs['printCMD']:
        print ''
    if kwargs['verbose']:
        print '=== ENV VARS'
        for v in ENVVARS:
            print '%s=%s' % (v, os.environ[v])

    envvarCMD = ' '.join(["-v %s" % v for v in ENVVARS])
    outlogfile = os.path.join(os.environ['BNPYLOGDIR'], '$JOB_ID.$TASK_ID.out')
    errlogfile = os.path.join(os.environ['BNPYLOGDIR'], '$JOB_ID.$TASK_ID.err')
    
    if kwargs['algName'].count('par') == 0  or kwargs['nWorkers'] <= '1':
        parallelCMD = ''
    else:
        parallelCMD = '-pe smp %s' % (kwargs['nWorkers'])
        # Request multiple cores for at most 5 days
        #parallelCMD = '-pe smp %s -l h_rt=120:00:00' % (kwargs['nWorkers'])


    print '>>>  %s' % (CMDkwargs['jobname'])

    if kwargs['qmachine'] is None or kwargs['qmachine'] == 'none':
        # Specify with "-l long" syntax
        qSpec = "-l %s" % (kwargs['queue'])
    else:
        # Specify with "-l long -q '*@@liv'" syntax
        qSpec = "-l %s -q '*@@%s'" % (kwargs['queue'], kwargs['qmachine'])

    if XHOST.count('grid') and not XHOST.count('test'):
        qsubCMD = "qsub -t 1-%s %s %s %s %s"\
            % (CMDkwargs['nTask'], qSpec,
               parallelCMD,
               envvarCMD,
               CMD)
        print qsubCMD
        status, output = commands.getstatusoutput(qsubCMD)
        print status
        print output
    elif XHOST.count('testgrid'):
        qsubCMD = "qsub -t 1 -l test %s %s %s"\
            % (parallelCMD,
               envvarCMD,
               CMD)
        print qsubCMD
        status, output = commands.getstatusoutput(qsubCMD)
        print status
        print output
        return
    elif XHOST.count('local'):
        CMD = "python " + CMD
        print CMD
        proc = subprocess.Popen(CMD.split(), shell=False)
        proc.wait()
    else:
        if CMDkwargs['printCMD']:
            print CMD
    if CMDkwargs['printCMD']:
        print ''


def makeJobCMD(algName='bnpyHDPmemo',
               **kwargs):
    """ Make string executable cmd for the current job.

    Creates a key/value dict of settings in 3 steps:
    1) Load algorithm settings from settings-$algName.txt file.
    2) Update these settings from data-settings-$dataName.txt file.

    Returns
    -------
    CMD : str
        This string is executable.
    """
    kwargs['algName'] = algName

    # Load settings from text file
    algfilepath = "settings-%s.txt" % (algName)
    CMDkwargs, algDollarKeys = loadKwargMapFromTextFile(algfilepath)

    datafilepath = "data-settings-%s.txt" % (kwargs['dataName'])
    dataKwargs, dataDollarKeys = loadKwargMapFromTextFile(datafilepath)
    CMDkwargs.update(dataKwargs)
    # Update settings with provided kwargs
    #for key in CMDkwargs:
    #    if key in kwargs:
    #        CMDkwargs[key] = kwargs[key]
    CMDkwargs.update(kwargs)

    # Substitute in "_test" kwargs so that testing is much faster.
    for key, val in CMDkwargs.items():
        if key.endswith('_test'):
            if kwargs['XHOST'].count('test'):
                targetkey = key.replace('_test', '')
                CMDkwargs[targetkey] = val
            del CMDkwargs[key]

    if CMDkwargs['initname'].count('repeattrue'):
        CMDkwargs['K'] = str(int(CMDkwargs['Ktrue']) * 2)
    elif CMDkwargs['initname'].count('true'):
        CMDkwargs['K'] = CMDkwargs['Ktrue']
        if not algName.count('createanddestroy'):
            CMDkwargs['nTask'] = '1'

    # Update any kwargs marked with dollar signs
    # BEFORE: CMDkwargs['jobname'] = $nickName-alg=$algName-lam=$lam
    # AFTER:  CMDkwargs['jobname'] = abc-alg=bnpyHDPmemo-lam=0.1 
    CMDkwargs = updateKwargsAndOSEnvWithDollarKeys(CMDkwargs, algDollarKeys)
    CMDkwargs = updateKwargsAndOSEnvWithDollarKeys(CMDkwargs, dataDollarKeys)

    if 'jobname_suffix' in CMDkwargs:
        CMDkwargs['jobname'] += CMDkwargs['jobname_suffix']

    # Create the "main" command.
    # Including all positional args, but no kwargs.
    if 'dataPath' in CMDkwargs:
        dataArg = CMDkwargs['dataPath']
    else:
        dataArg = CMDkwargs['dataName']

    if algName.count('bnpy'):
        CMD = "./Run_bnpy.py %s %s %s %s" % (
            dataArg,
            CMDkwargs['allocModelName'],
            CMDkwargs['obsModelName'],
            CMDkwargs['bnpy_algName'])

        del CMDkwargs['allocModelName']
        del CMDkwargs['obsModelName']
        del CMDkwargs['bnpy_algName']
        del CMDkwargs['nickName']
        del CMDkwargs['algName']
    elif algName.count('sampler'):
        CMD = "./Run_foxHDPHMMsampler.py"
        del CMDkwargs['nickName']
        del CMDkwargs['algName']
    else:
        raise NotImplementedError('TODO')

    # Add all keyword args
    for key, val in CMDkwargs.items():
        if key.isupper() and len(key) > 1:
            continue # skip ENVVAR keys
        if key.count('nTask') and kwargs['XHOST'].count('grid'):
            # Dont let a run submitted to the grid do any more than one task
            continue
        CMD += " --%s %s" % (key, val)
    return CMD, CMDkwargs

def loadKwargMapFromTextFile(filepath):
    # Load dataset settings from text file
    CMDkwargs = OrderedDict()
    dollarKeys = dict()
    with open(filepath, 'r') as f:
        for line in f.readlines():
            line = line.strip()
            fields = line.split(" ")
            if len(fields) < 2:
                continue
            key = fields[0]
            val = fields[1]
            CMDkwargs[key] = val
            if val.count("$"):
                partList = val.replace('=', '-').replace(os.path.sep, '-').split('-')
                dollarKeys[key] = [x for x in partList if x.startswith('$')]
    return CMDkwargs, dollarKeys

def updateKwargsAndOSEnvWithDollarKeys(CMDkwargs, dollarKeys):
    for key, partList in dollarKeys.items():
        val = CMDkwargs[key]
        for pStartDollar in partList:
            if pStartDollar.isupper() and len(pStartDollar[1:]) > 1:
                val = val.replace(pStartDollar, os.environ[pStartDollar[1:]])
            else:
                val = val.replace(pStartDollar, CMDkwargs[pStartDollar[1:]])
        if key.isupper() and len(key) > 1:
            os.environ[key] = val
            if CMDkwargs['printCMD']:
                print "export %s=%s" % (key, val)
        else:
            CMDkwargs[key] = val
    # Handle options that require simple if logic
    for key, targetval in CMDkwargs.items():
        if key.count('_if_'):
            ifFields = key.split('_if_')
            targetkey = ifFields[0]
            eqFields = ifFields[1].split('=')
            reqkey = eqFields[0].replace('$', '')
            reqval = eqFields[1]
            if reqkey not in CMDkwargs:
                continue
            if targetkey + '_final' in CMDkwargs:
                CMDkwargs[targetkey] = CMDkwargs[targetkey + '_final']
            elif CMDkwargs[reqkey] == reqval:
                CMDkwargs[targetkey] = targetval
            del CMDkwargs[key]
    return CMDkwargs

def arglist_to_dict(argList):
    ''' Transform list of name/val pairs into keyword dict.
    '''
    L = len(argList)
    kwargs = dict()
    for pos in xrange(0, L, 2):
      name = argList[pos][2:]
      val = argList[pos+1]
      kwargs[name] = val
    return kwargs

if __name__ == '__main__':
    main()



