#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName SpeakerDiar \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --meetingNum $meetingNum \
 --nickName nips2015 \
 --nTask 10 \
"""

hmmKappa = 100
K = 25
algName = 'bnpyHDPHMMmemo'

for meetingNum in range(1,21+1):
    # Execute the command
    curCMD = CMD + ""
    curCMD = curCMD.replace('$meetingNum', str(meetingNum))    
    curCMD = curCMD.replace('$K', str(K))
    curCMD = curCMD.replace("$algName", algName)
    curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
    proc = subprocess.Popen(curCMD.split(), shell=False)
    proc.wait()

