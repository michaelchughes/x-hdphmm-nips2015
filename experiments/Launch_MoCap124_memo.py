#!/usr/bin/python

import subprocess

CMD = """
python LaunchRun.py \
 --dataName MoCap124 \
 --K $K \
 --hmmKappa $hmmKappa \
 --algName $algName \
 --nickName nipsexperiment \
 --nTask 5 \
"""

for hmmKappa in [100]:
    for K in [100, 200]:
        for algName in ['bnpyHDPHMMmemo']:

            # Execute the command
            curCMD = CMD + ""
            curCMD = curCMD.replace('$K', str(K))
            curCMD = curCMD.replace("$algName", algName)
            curCMD = curCMD.replace("$hmmKappa", str(hmmKappa))
            proc = subprocess.Popen(curCMD.split(), shell=False)
            proc.wait()

