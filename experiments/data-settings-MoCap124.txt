N 124
Ktrue 0
maxT 1483
BNPYDATADIR $XHMMROOT/datasets/$dataName/

nBatch 20
minLaps 400
nLap 1000
nLap_test 5

saveEvery 10
saveEveryLogScaleFactor 2

obsModelName AutoRegGauss
ECovMat diagcovfirstdiff
sF 0.5
VMat same
sV 0.5
MMat eye

doFullPassBeforeMstep 50

deleteFailLimit 15
dtargetMaxSize 20

creationProposalName bisectGrownBlocks
creationNumProposal_early 5
creationNumProposal_late 1
creationLapDelim_early 50
creationLapDelim_late 200
creationKfresh_early 2
creationKfresh_late 2
minLaps 400
minBlockSize 10 
maxBlockSize 200
growthBlockSize 25
nRefineIters 3

nGlobalIters 1
nGlobalItersBigChange 5

